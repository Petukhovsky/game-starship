// =============================================================
//     ===== CANVAS =====
//     ===== animation ====
// script written by Petukhovsky
// and Mark Korneychik
// http://petukhovsky.com
// 
// 2022.05.03 - moved to Gitlab 
// =============================================================

"use strict"; //All my JavaScript written in Strict Mode http://ecma262-5.com/ELS5_HTML.htm#Annex_C

(function(){
    // ======== private vars ========
    let canvas, ctx; //Объекты canvas, context
    let canvasbuf;

    const W = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    const H = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    let contwidth = 360; //Ширина контейнера
    let contheight = contwidth / W * H; //Высота контейнера
    console.log(contheight);

    let requestAnimFrame; //Функция анимации

    let load = {point: 0, total: 4}; //всего загружаем 2 элемента и 1 для подключения

    let game;

    let imgbase, imganim, imgmap, imgstar, imgmeteor;

    let fpsobject = {type: "text", x: 16, y: 1, text: "FPS: "};
    let scoreobject = {type: "text", x: 16, y: 16, text: "SCORE: "};
    let heartsobject = {type: "text", x: 16, y: 50, text: "❤️️️️️"};

    const ui_font_color = "#4f4844"; //Цвет шрифта по умолчанию
    const ui_font_style = "9pt Verdana"; //([font style][font weight][font size][font face]).
    const ui_font_warning_color = "#ff0000"; //Красный
    const contcolor = "#3d3d3d"; //Цвет заливки
    const clientver = "v.0.1.1";
    const htmlid = "pejs-game";
    const srvaddress = "";
    const graphicsdir = srvaddress + "img/";

    class Point {
        constructor(x, y) {
            this.x = x || 0;
            this.y = y || 0;
        }

        static add(a, b) {
            return new Point(a.x + b.x, a.y + b.y);
        }

        static sub(a, b) {
            return new Point(a.x - b.x, a.y - b.y);
        }

        static scale(v, s) {
            return v.clone().scale(s);
        }

        add(v) {
            this.x += v.x;
            this.y += v.y;
            return this;
        }

        sub(v) {
            this.x -= v.x;
            this.y -= v.y;
            return this;
        }

        scale(s) {
            this.x *= s;
            this.y *= s;
            return this;
        }

        clone() {
            return new Point(this.x, this.y);
        }

        get length() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }

        toLength(newLength) {
            let curLength = this.length;
            this.scale(newLength / curLength);
            return this;
        }

        normalize() {
            return this.toLength(1);
        }

        normalized() {
            return this.clone().normalize();
        }

        static distance(a, b) {
            return a.clone().sub(b).length;
        }

        rotate(angle) {
            let newX = this.x * Math.cos(angle) - this.y * Math.sin(angle);
            let newY = this.x * Math.sin(angle) + this.y * Math.cos(angle);
            this.x = newX;
            this.y = newY;
            return this;
        }

        toString() {
            return "(x:" + this.x + ", y: " + this.y + ")";
        }
    }

    class Rectangle {
        constructor(props) {
            // {
            //     width, - ширина
            //     height, - высота
            //     center: Point, - центр прямоугольника
            //     v: Point - вектор направления
            //     color:
            //     image: {
            //
            //     }
            // }
            // (x1, y1), (x3, y3) противоположные точки диагонали
            if (true) {
                let x1 = - props.width / 2;
                let y1 = - props.height / 2;

                let x2 = + props.width / 2;
                let y2 = - props.height / 2;

                let x3 = + props.width / 2;
                let y3 = + props.height / 2;

                let x4 = - props.width / 2;
                let y4 = + props.height / 2;

                this.points = [
                    new Point(x1, y1),
                    new Point(x2, y2),
                    new Point(x3, y3),
                    new Point(x4, y4),
                ];
            }

            // настройки
            const defaultSettings = {
                CYCLIC_DRAW: false, // если true то будет рисоваться копия
                GAME_OVER_WHEN_OUT_OF_MAP: false,
                DRAW_PATH: false,
            }
            this.settings = {}
            Object.assign(this.settings, defaultSettings, props.settings);

            this.center = new Point(props.center.x, props.center.y);

            this.v = props.v;
            this.color = props.color;
            this.speed = props.speed;
            this.rotationSpeed = props.rotationSpeed;  // time in seconds to rotate one circle
            this.rotateDirection = 0;
            this.lastUpdate = (new Date()).getTime();
            this.image = props.image;
        }

        point(number) {
            if (number < 0 || number > 3) return {x: 0, y: 0};
            return new Point(this.center.x + this.points[number].x, this.center.y + this.points[number].y);
        }

        static getDirectionAngle(v) {
            let angle = Math.atan2(v.y, v.x);
            return angle - Math.atan2(-1, 0);
        }

        // рисуем прямоугольник с заданным центром
        drawWithCenter(center, image) {
            let directionAngle = Rectangle.getDirectionAngle(this.v);

            let width = this.points[2].x - this.points[0].x;
            let height = this.points[2].y - this.points[0].y;

            canvasbuf.ctx.save();
            canvasbuf.ctx.translate(this.point(0, center).x, this.point(0, center).y);
            canvasbuf.ctx.translate(width / 2, height / 2);
            canvasbuf.ctx.rotate(directionAngle);
            canvasbuf.ctx.drawImage(image || imganim, this.image.x, this.image.y, this.image.width, this.image.height, -width / 2, -height / 2, width, height);
            canvasbuf.ctx.restore();
        }
    }

    class Airplane extends Rectangle {
        constructor(props) {
            super(props);
            this.toDraw = [];
        }

        get width() {
            return Math.trunc(Math.abs(this.points[0].x - this.points[1].x));
        }

        update() {
            let currentTime = (new Date()).getTime();

            this.center.x += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().x;
            this.center.y += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().y;

            if (this.center.x < 0) this.center.x += contwidth;
            if (this.center.x > contwidth) this.center.x -= contwidth;

            this.speed += (currentTime - this.lastUpdate) / 1000;

            this.v.rotate(
                Math.PI * 2
                * ((currentTime - this.lastUpdate) / 1000 / this.rotationSpeed)
                * this.rotateDirection
            );

            let directionAngle = Rectangle.getDirectionAngle(this.v);

            if (this.settings.GAME_OVER_WHEN_OUT_OF_MAP) { // проверяем что прямоугольник находится вне карты

                // так как прямоугольник не всегда прямоугольник то нужно приближение для проверки что содержимое прямоугольника вышло за границы
                //
                // проверяем что середины сторон прямоугольника вышли за границы
                let last = this.points[0].clone().rotate(directionAngle).add(this.center);
                for (let i = 1; i < 5; i++) {
                    let cur = this.points[i % 4].clone().rotate(directionAngle).add(this.center);
                    last.add(cur).scale(1 / 2);

                    if (last.y > contheight || (!this.settings.CYCLIC_DRAW && (last.x < 0 || last.x > contheight))) {
                        console.log(last.y);
                        game.events.push({ type: "GAME_OVER" });
                        break;
                    }
                    last = cur;
                }
            }

            if (this.center.y < contheight * 2 / 3) {
                game.events.push({
                    type: "MOVE_MAP",
                    payload: {
                        height: contheight * 2 / 3 - this.center.y,
                    }
                });
                this.center.y = contheight * 2 / 3;
            }

            if (this.settings.DRAW_PATH) {
                let p2 = this.points[2].clone().rotate(directionAngle).add(this.center);
                let p3 = this.points[3].clone().rotate(directionAngle).add(this.center);
                let p = Point.add(p2, p3).scale(1 / 2);

                this.toDraw.push({
                    mapHeightSnapshot: game.map.height,
                    x: p.x,
                    y: p.y,
                    color: "#414c54",
                    opacity: 200,
                    updated: (new Date()).getTime(),
                    update() {
                        let curTime = (new Date()).getTime();
                        this.opacity -= curTime - this.updated;
                        this.updated = curTime;
                    },
                    draw() {
                        canvasbuf.ctx.save();
                        canvasbuf.ctx.beginPath();
                        canvasbuf.ctx.strokeStyle = this.color;
                        canvasbuf.ctx.fillStyle = this.color;
                        canvasbuf.ctx.globalAlpha = this.opacity / 200;
                        canvasbuf.ctx.arc(this.x, this.y - this.mapHeightSnapshot + game.map.height, 1, 0, Math.PI * 2, true);
                        canvasbuf.ctx.fill();
                        canvasbuf.ctx.stroke();
                    }
                });

                for (let i = 0; i < this.toDraw.length; i++) {
                    this.toDraw[i].update();
                }
                while (this.toDraw.length > 0 && this.toDraw[0].opacity <= 0) {
                    this.toDraw.shift();
                }
            }

            this.lastUpdate = currentTime;
        }

        // рисуем прямоугольник
        draw() {
            for (let i = 0; i < this.toDraw.length; i++) {
                this.toDraw[i].draw();
            }
            this.drawWithCenter(this.center);
            // если карта зацикленна, то отрисовываем копию прямоугольника
            if (this.settings.CYCLIC_DRAW) {
                if (this.center.x < contwidth / 2) {
                    this.center.x += contwidth;
                    this.drawWithCenter(this.center);
                    this.center.x -= contwidth;
                } else {
                    this.center.x -= contwidth;
                    this.drawWithCenter(this.center);
                    this.center.x += contwidth;
                }
            }
        }
    }

    class Star extends Rectangle {
        constructor(props) {
            super(props);
            this.mapHeightSnapshot = game.map.height;
            this.speed = 500;
            this.active = true;
        }

        get width() {
            return Math.trunc(Math.abs(this.points[0].x - this.points[1].x));
        }

        update() {
            this.center.y += game.map.height - this.mapHeightSnapshot;
            this.mapHeightSnapshot = game.map.height;

            let distance = Point.distance(this.center, game.airplane.center);
            if (distance < 100) {
                if (distance < 5 && this.active) {
                    this.active = false;
                    game.score += this.width;
                }
                this.v = Point.sub(game.airplane.center, this.center);
                this.speed = game.airplane.speed * 1.5;
            } else {
                this.speed = 0;
            }

            if (this.center.y > contheight) {
                this.active = false;
            }

            let currentTime = (new Date()).getTime();

            this.center.x += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().x;
            this.center.y += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().y;

            this.lastUpdate = currentTime;
        }

        draw() {
            this.drawWithCenter(this.center, imgstar);
        }
    }

    class Meteor extends Rectangle {
        constructor(props) {
            super(props);
            this.mapHeightSnapshot = game.map.height;
            this.active = true;
        }

        get width() {
            return Math.trunc(Math.abs(this.points[0].x - this.points[1].x));
        }

        update() {
            this.center.y += game.map.height - this.mapHeightSnapshot;
            this.mapHeightSnapshot = game.map.height;

            let distance = Point.distance(this.center, game.airplane.center);
            if (distance < (this.width / 2 + game.airplane.width / 2) * 0.8 && this.active) {
                this.active = false;
                game.dispatch({ type: 'GAME_OVER' });
                this.v = Point.sub(game.airplane.center, this.center);
            }

            if (this.center.y > contheight) {
                this.active = false;
            }

            let currentTime = (new Date()).getTime();

            this.center.x += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().x;
            this.center.y += (currentTime - this.lastUpdate) / 1000 * this.speed * this.v.normalize().y;

            this.lastUpdate = currentTime;
        }

        draw() {
            this.drawWithCenter(this.center, imgmeteor);
        }
    }

    class Map{
        constructor(props) {
            this.height = 0;
            this.objects = [];
        }

        setHeight(newHeight) {
            this.height = newHeight;
        }

        addHeight(heightDiff) {
            this.height += heightDiff;
        }

        update() {
            if (Math.trunc(Math.random() * 30) === 0) {
                this.objects.push(new Star({
                    center: new Point(Math.random() * contwidth, - 100),
                    width: 55 / 2 + (Math.random() * 10 - 5),
                    height: 55 / 2 + (Math.random() * 10 - 5),
                    color: "#000",
                    speed: 0,
                    rotationSpeed: 1.2,
                    v: new Point(Math.random() * 2 - 1, Math.random() * 2 - 1),
                    image: {
                        x: 0,
                        y: 0,
                        width: 55,
                        height: 53,
                    },
                }));
            }
            if (Math.trunc(Math.random() * Math.max(1, 30 - 2 * Math.log2(game.getScore()))) === 0) {
                this.objects.push(new Meteor({
                    center: new Point(-50 + Math.random() * (contwidth + 100), - 100),
                    width: 55 / 2 - 5 + (Math.random() * 20),
                    height: 55 / 2 - 5 + (Math.random() * 20),
                    color: "#000",
                    speed: Math.random() * 50,
                    rotationSpeed: 1 + Math.random(),
                    rotateDirection: (Math.random() * 2 - 1 < 0 ? -1 : 1),
                    v: new Point(Math.random() * 2 - 1, 1),
                    image: {
                        x: 0,
                        y: 0,
                        width: 205,
                        height: 218,
                    },
                }));
            }
            console.log(this.objects.length);
            this.objects = this.objects.filter(obj => {
                if (obj.active === false) return false;
                return true;
            })
            for (let i = 0; i < this.objects.length; i++) {
                this.objects[i].update();
            }
        }

        calculateXFromHeight() {
            const sHeight = 1080;

            let x = this.height;
            x -= Math.trunc(x / 1280) * 1280;
            return 2560 - x - sHeight - (1280 - sHeight);
        }

        draw() {
            let sWidth = 1080 / contheight * contwidth, sHeight = 1080;
            let sx = 720 / 2 - sWidth / 2, sy = this.calculateXFromHeight(this.height);
            canvasbuf.ctx.drawImage(imgmap, sx, sy, sWidth, sHeight, 0, 0, contwidth, contheight);
            for (let i = 0; i < this.objects.length; i++) {
                this.objects[i].draw();
            }
        }
    }

    class Game {
        constructor() {
            this.state = {
                // Состояния приложения  -1 (load) - приложение грузится, 0 (startmenu) - загрузка прошла, 1(run) - работа, 2(menu) - меню, 3(over) - работа окончена
                appstatus: "load",
            }

            this.fps = 0;
            this.eventListeners = [];
            this.events = [];
            this.score = 0;
        }

        getScore() {
            return Math.trunc(this.map.height / 100) + this.score;
        }

        addEventListener(who, event, callback, logging = true) {
            const todo = (e) => {
                this.events.push(() => {
                    if (logging) console.log(event);
                    callback(e);
                });
            };

            who.addEventListener(event, todo);
            this.eventListeners.push({ event, todo });
        }

        get appstatus() {
            return this.state.appstatus;
        }

        set appstatus(status) {
            this.setAppStatus(status);
        }

        setAppStatus(status) {
            switch (status) {
                case "startmenu":
                    this.draw_welcome();
                    break;
                case "run":
                    this.animation_run();
                    break;
            }

            this.state.appstatus = status;

            console.log(status);
        }

        animation_run() {
            if (this.appstatus != "startmenu") return; //Загрузка не прошла или мы по середине игры то не стартуем

            this.starttime = (new Date()).getTime();

            document.getElementById(htmlid+"-div").style.display = "none";

            // Создаём буфер нужного размера:
            canvasbuf = document.createElement("canvas");
            canvasbuf.width = contwidth;
            canvasbuf.height = contheight;
            canvasbuf.ctx = canvasbuf.getContext("2d");
            //canvasbuf.ctx.fillRect(0,0,660,400);

            canvasbuf.ctx.fillStyle = contcolor; // цвет фона
            canvasbuf.ctx.strokeStyle = '#020'; //цвет линий
            canvasbuf.ctx.fillRect(0, 0, canvas.width, canvas.height);
            canvasbuf.ctx.lineWidth = 2; // ширина линий


            // ---- fps count ----
            setInterval(() => {
                fpsobject.text = "" + (this.fps * 2);
                this.fps = 0;
            }, 500); // update every 1/2 seconds

            this.map = new Map();

            this.airplane = new Airplane({
                center: new Point(contwidth / 2, contheight - 80),
                width: 30,
                height: 45,
                color: "#000",
                speed: 220,
                rotationSpeed: 1.2,
                v: new Point(0, -1),
                image: {
                    x: 0,
                    y: 7,
                    width: 40,
                    height: 60,
                },
                settings: {
                    CYCLIC_DRAW: true,
                    GAME_OVER_WHEN_OUT_OF_MAP: true,
                    DRAW_PATH: true,
                }
            });

            this.addEventListener(canvas, 'mousedown', e => {
                e.preventDefault();
                let mousePos = this.getMousePos(canvas, e);
                this.airplane.rotateDirection = (mousePos.x > contwidth / 2 ? 1 : -1);
            });

            this.addEventListener(canvas, 'touchstart', e => {
                e.preventDefault();
                let mousePos = this.getTouchPos(canvas, e);
                console.log(mousePos.x, mousePos.y);
                this.airplane.rotateDirection = (mousePos.x > contwidth / 2 ? 1 : -1);
            });

            this.addEventListener(canvas, 'mouseup', e => {
                e.preventDefault();
                this.airplane.rotateDirection = 0;
            });

            this.addEventListener(canvas, 'touchend', e => {
                e.preventDefault();
                this.airplane.rotateDirection = 0;
            });
        };

        getMousePos(canvas, evt) {
            // let rect = canvas.getBoundingClientRect();
            return new Point(
                evt.clientX / window.screen.width * canvas.width,
                evt.clientY / window.screen.height * canvas.height,
            );
        }

        getTouchPos(canvasDom, touchEvent) {
            return {
                x: touchEvent.touches[0].clientX / window.screen.width * canvas.width,
                y: touchEvent.touches[0].clientY / window.screen.height * canvas.height,
            };
        }

        dispatch(action) {
            switch (action.type) {
                case "MOVE_MAP":
                    game.map.addHeight(action.payload.height);
                    return;
                case "GAME_OVER":
                    this.setAppStatus('game_over');
                    console.log("GAME_OVER");
                    return;
            }
        }

        update() {
            while (this.events.length !== 0) {
                let event = this.events.shift();
                if (typeof event === "function") {
                    event();
                } else {
                    this.dispatch(event);
                }
            }
            this.map.update();
            this.airplane.update();
        }

        resize() {

        }

        draw() {
            // console.log("draw");
            this.fps++;

            if (this.appstatus === "run") {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                canvasbuf.ctx.clearRect(0, 0,canvas.width, canvas.height);

                this.map.draw();
                this.airplane.draw();

                //Копирование поля с клетками из буфера
                ctx.drawImage(canvasbuf, 0, 0);

                ctx.fillStyle = ui_font_color; //Устанавливаем цвет шрифта
                ctx.font = ui_font_style;      //Устанавливаем стиль шрифта
                ctx.textBaseline = "top";      //Текст выравнивается по верхнему углу
                ctx.textAlign = "left";
                //Вывод информации о времени ответа, версии клиента и прочем
                this.drawobj(Object.assign({}, fpsobject));
                ctx.restore();

                scoreobject.text = "SCORE: " + game.getScore();
                this.drawobj(Object.assign({}, scoreobject, {color: "#ffffff", style:"20px Verdana"}));
                this.drawobj(Object.assign({}, heartsobject, {color: "#d60000", style:"20px Verdana"}));
            }

            ctx.restore();
        }

        draw_welcome() {
            document.getElementById(htmlid + "-div").style.display = "inline";
            document.getElementById(htmlid + "-run").disabled = false;

            const logo = {type: "img", x: 100, y: 30, ox: 603, oy: 23, w: 468, h: 55};
            const logotext = { type: "text", x: 240, y: 150, text: "Press RUN to start" };

            ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
            ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

            //Вывод welcome
            this.drawobj(logo);

            ctx.textAlign = "left";
            this.drawobj(logotext);
        };

        undef(obj) {
            return typeof obj === "undefined";
        };

        def(obj) {
            return !undef(obj);
        };

        drawobj(_obj) { //Рисоваие объекта по собственным координатам

            //Вывод объекта
            if (_obj.type == "img") {
                ctx.drawImage(imgbase, //картинка
                    _obj.ox, _obj.oy,
                    _obj.w, _obj.h,
                    _obj.x, _obj.y, //X,Y
                    _obj.w, _obj.h
                );
            } else if (_obj.type == "text") {
                ctx.save();
                if (this.undef(_obj.color)) ctx.fillStyle = ui_font_color;
                else ctx.fillStyle = _obj.color;

                if (this.undef(_obj.style)) ctx.font = ui_font_style;
                else ctx.font = _obj.style;

                ctx.fillText(_obj.text, _obj.x, _obj.y);
                ctx.restore();
            }
        };
    }

    ////////////////////////////////////////////////////////////////////////////
    const init = () => {
        console.log(window.screen.height, window.screen.width);
        load.point = 0;
        load.total = 3; //всего загружаем 2 элемента + 1 статус что всё ОК

        game = new Game();

        document.getElementById(htmlid+"-div").style.display = "none";
        document.getElementById(htmlid+"-run").disabled = true;
        document.getElementById(htmlid + "-run").onclick = () => {
            game.setAppStatus('run');
            animloop();
        }

        console.log("init function");

        // ---- init script ----
        canvas = document.getElementById(htmlid + "-canvas"); //Хватаем объект
        canvas.width = contwidth; //Задаём ширину холста
        canvas.height = contheight;  //Задаём ширину холста
        ctx = canvas.getContext("2d"); //Инициализируем работу с контекстом. Контекст позволяет работать с canvas применяя методы рисования

        // Если ничего нет - возвращаем обычный таймер
        requestAnimFrame = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };

        console.log("init OK");
        load_app();

    };

//////////////////////////////ANIMATION LOGIC/////////////////////////////////////

    const load_app = () => {
        imgbase = new Image();	// Создание нового объекта изображения
        imgbase.src = graphicsdir + "base.png";	// Путь к изображениям
        imgbase.onload = () => {
            drawloading();   // Событие onLoad, ждём момента пока загрузится изображение
        };

        imganim = new Image();
        imganim.src = graphicsdir + "anim.png";
        imganim.onload = () => {
            drawloading();   // Событие onLoad, ждём момента пока загрузится изображение
        };

        imgmap = new Image();
        imgmap.src = graphicsdir + "map.png";
        imgmap.onload = () => {
            drawloading();   // Событие onLoad, ждём момента пока загрузится изображение
        };

        imgstar = new Image();
        imgstar.src = graphicsdir + "star.png";
        imgstar.onload = drawloading;

        imgmeteor = new Image();
        imgmeteor.src = graphicsdir + "meteor.png";
        imgmeteor.onload = drawloading;

        /*
        imgpart = new Image();
        imgpart.src = graphicsdir + "part.png";
        imgpart.onload = drawloading;
        */
    };

    //Выполняем по завершении загрузки

    ////////////////////////////////////////////////////////////////////////////
    const drawloading = () => {
        load.point++;

        ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
        ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

        ctx.fillStyle = ui_font_color;

        //Рисование прогресса загрузки
        ctx.strokeRect(Math.floor(contwidth / 4), Math.floor(contheight / 2 - 6), Math.floor(contwidth / 2), 12);
        ctx.fillRect(Math.floor(contwidth / 4 + 1), Math.floor(contheight / 2 - 4), Math.floor(contwidth / 2 / load.total * load.point), 8);

        console.log("drawloading  OK");

        //console.log("load.point = "+load.point);
        if (load.point >= load.total) {
            //console.log("load.point OK");
            game.setAppStatus('startmenu');
        }
    };


    // var drawobjc = function (_obj, _x, _y) { //Рисоваие объекта по координатам из параметров
    //     _obj.x = _x;
    //     _obj.y = _y;
    //     drawobj(_obj);
    // };

    // var drawobjx = function (_obj, _x) {
    //     _obj.x = _x;
    //     drawobj(_obj);
    // };
    // var drawobjy = function (_obj, _y) {
    //     _obj.y = _y;
    //     drawobj(_obj);
    // };

    /*
    var random = function (min, max) {
        return Math.random() * (max - min) + min;
    }

    var xabs = function (_x) {
        if (_x > -2 && _x < 11) return _x;
        if (_x + mapsizex > -2 && _x + mapsizex < 11) return _x + mapsizex;
        return _x - mapsizex;
    }

    var yabs = function (_y) {
        if (_y > -2 && _y < 11) return _y;
        if (_y + mapsizey > -2 && _y + mapsizey < 11) return _y + mapsizey;
        return _y - mapsizey;
    }

    var inv_button_active = function () {
        var count = 0;
        for (var i = 0; i < inventory.size; i++) {
            if (def(inventory.obj[i]) && inventory.obj[i].type == this.cond1) count += inventory.obj[i].num;
        }
        return count >= this.cond2;
    }

    var obj_button_active = function () {
        var count = 0;
        for (var i = 0; i < objinv.len; i++) {
            if (def(objinv.obj[i]) && objinv.obj[i].type == this.cond1) count += objinv.obj[i].num;
        }
        return count >= this.cond2;
    }

    var clone = function (obj) {
        if (obj == null || typeof(obj) != "object")
            return obj;

        var temp = obj.constructor();

        for (var key in obj)
            temp[key] = clone(obj[key]);
        return temp;
    }

    */

    //////////////////////////////MAIN LOOP/////////////////////////////////////
    const processloop = () => {
        if (game.appstatus != "run" && game.appstatus != "menu") return; //Не выполнять, если приложение не запущено

        // if ((new Date()).getTime() > starttime + animtime) { //Анимация завершена
        //     game.setAppStatus("startmenu");
        //     console.log("ANIMATION IS OVER");
        //     setTimeout(run_startmenu, 100);
        // }

        if (game.appstatus == "run" || game.appstatus == "menu") setTimeout(processloop, 500);
    };

    const animloop = () => {
        game.update();
        game.draw();

        if (game.appstatus === "run" || game.appstatus === "menu") {
            requestAnimFrame(animloop);
        }
    };

    return {
        ////////////////////////////////////////////////////////////////////////////
        // ---- onload event ----
        load: () => {
            console.log("load function");
            window.addEventListener("load", () => {
                init();
            }, false);
        }
    }
})().load();

